from functools import partial

from skimage.filters import median
from skimage.morphology import disk

from tissue_segmentation.processing.channels import grayscale, rgb_average, rgb2entropy, colordistance, saturation

_CHANNELS = {
    "Grayscale": grayscale,
    "RGBAvg": rgb_average,
    "Saturation": saturation,
    "ColorDist": colordistance,
    "LocalEntropy": rgb2entropy
}

median_fn = partial(median, footprint=disk(5))
