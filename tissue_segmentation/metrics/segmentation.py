import logging
from typing import Callable, Dict, List

import numpy as np
from scipy.spatial import cKDTree
from skimage.measure import find_contours

SimilarityMetric = Callable[[np.ndarray, np.ndarray], float]


def require_boolean(fn: SimilarityMetric) -> SimilarityMetric:
    """
    Decorator checking that the functions receive binary masks
    :param fn:
    :return:
    """
    def decorated_fn(im1: np.ndarray, im2: np.ndarray) -> float:
        if im1.dtype != bool or im2.dtype != bool:
            msg = f"Metric must be computed on binary masks. Types received were {im1.dtype} and {im2.dtype}"
            logging.error(msg)
            raise ValueError(msg)

        return fn(im1, im2)

    return decorated_fn


@require_boolean
def iou(im1: np.ndarray, im2: np.ndarray) -> float:
    """
    Intersectino over Union
    :param im1:
    :param im2:
    :return:
    """
    i = (im1 & im2).sum()
    u = (im1 | im2).sum()
    return i/u


@require_boolean
def dsc(im1: np.ndarray, im2: np.ndarray) -> float:
    """
    Dice Similarity Coefficient
    :param im1:
    :param im2:
    :return:
    """
    i = (im1 & im2).sum()
    s = im1.sum() + im2.sum()
    return 2*i/s


def _get_distances(im1: np.ndarray, im2: np.ndarray) -> np.ndarray:
    """
    Get the distances between the contours of the two masks
    :param im1:
    :param im2:
    :return:
    """
    p1 = np.concatenate(find_contours(im1))
    p2 = np.concatenate(find_contours(im2))

    if len(p1) == 0:
        return np.array([0]) if len(p2) == 0 else np.array([np.inf])
    elif len(p2) == 0:
        return np.array([np.inf])

    return np.concatenate([cKDTree(p1).query(p2, k=1)[0], cKDTree(p2).query(p1, k=1)[0]])


@require_boolean
def asd(im1: np.ndarray, im2: np.ndarray) -> float:
    """
    Average Surface Distance
    :param im1:
    :param im2:
    :return:
    """
    return _get_distances(im1, im2).mean()


@require_boolean
def get_all_metrics(im1: np.ndarray, im2: np.ndarray) -> Dict[str, float]:
    return {
        "iou": iou(im1, im2),
        "dsc": dsc(im1, im2),
        "asd": asd(im1, im2)
    }
