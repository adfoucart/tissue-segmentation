"""
Descriptive metrics for tissue segmentation
"""
from typing import List, Tuple

import numpy as np


def separability(d1: np.ndarray, d2: np.ndarray) -> float:
    """
    Computes separability of the two distributions as $\frac{(µ_1-µ_2)^2}{s_1^2+s_2^2}$
    :param d1: 
    :param d2: 
    :return:
    """

    return ((d1.mean() - d2.mean()) ** 2) / (np.std(d1) ** 2 + np.std(d2) ** 2)


def get_fprs(values_background: np.ndarray, values_foreground: np.ndarray) -> List[float]:
    """
    Get FPR at different target FNR (from 0 to 10).

    Assumes that foreground values are supposed to be higher than background values.
    (e.g. saturation, inverse grayscale, etc.)

    The FPR@x is computed by finding the threshold t such that x% of foreground pixels would be labeled as background,
    then looking at the % of background pixels that would be labeled as foreground with that threshold.

    :param values_background:
    :param values_foreground:
    :return:
    """

    fprs = []
    for p in range(0, 11):
        t = np.percentile(values_foreground, p)  # threshold above which more than p% of positive pixels are missed
        fprs.append(
            (values_background > t).mean())  # % of background pixels which would be incorrectly added to foreground

    return fprs


def get_descriptive_metrics(im: np.ndarray, mask: np.ndarray, mask_trivial: np.ndarray) -> Tuple[float, List[float]]:
    """
    Retrieves separability and FPR@0:10 for the image and mask, ignoring all pixels in the mask_trivial mask.
    :param im:
    :param mask:
    :param mask_trivial: Mask with all the pixels at RGB = [255, 255, 255] in the image
    :return:
    """
    values_foreground = im[mask & (mask_trivial == False)]
    values_background = im[(mask == False) & (mask_trivial == False)]
    return separability(values_background, values_foreground), get_fprs(values_background, values_foreground)
