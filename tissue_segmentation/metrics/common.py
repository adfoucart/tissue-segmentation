from typing import List

import numpy as np
import scipy as sp
import scipy.stats as st
import itertools as it


def interquartiles_str(values: np.ndarray | List[float]) -> str:
    """
    Formatted string showing the median and interquartile range as: median [p25 - p75]
    :param values: list or ndarray of values in the distribution
    :return:
    """
    return f"{np.median(values):.2f} [{np.percentile(values, 25):.2f}-{np.percentile(values, 75):.2f}]"


def friedman_test(*args):
    """
    Performs a Friedman ranking test.
    Tests the hypothesis that in a set of k dependent samples groups (where k >= 2) at least two of the groups
    represent populations with different median values.

    From: I. Rodríguez-Fdez, A. Canosa, M. Mucientes, A. Bugarín, STAC: a web platform for the comparison of
    algorithms using statistical tests, in: Proceedings of the 2015 IEEE International Conference on Fuzzy
    Systems (FUZZ-IEEE), 2015.

    https://github.com/citiususc/stac

    Parameters
    ----------
    sample1, sample2, ... : array_like
        The sample measurements for each group.

    Returns
    -------
    F-value : float
        The computed F-value of the test.
    p-value : float
        The associated p-value from the F-distribution.
    rankings : array_like
        The ranking for each group.
    pivots : array_like
        The pivotal quantities for each group.

    References
    ----------
    M. Friedman, The use of ranks to avoid the assumption of normality implicit in the analysis of variance,
        Journal of the American Statistical Association 32 (1937) 674–701.
    D.J. Sheskin, Handbook of parametric and nonparametric statistical procedures. crc Press, 2003,
        Test 25: The Friedman Two-Way Analysis of Variance by Ranks
    """
    k = len(args)
    if k < 2: raise ValueError('Less than 2 levels')
    n = len(args[0])
    if len(set([len(v) for v in args])) != 1: raise ValueError('Unequal number of samples')

    rankings = []
    for i in range(n):
        row = [col[i] for col in args]
        row_sort = sorted(row)
        rankings.append([row_sort.index(v) + 1 + (row_sort.count(v) - 1) / 2. for v in row])

    rankings_avg = [sp.mean([case[j] for case in rankings]) for j in range(k)]
    rankings_cmp = [r / sp.sqrt(k * (k + 1) / (6. * n)) for r in rankings_avg]

    chi2 = ((12 * n) / float((k * (k + 1)))) * (
                (sp.sum(r ** 2 for r in rankings_avg)) - ((k * (k + 1) ** 2) / float(4)))
    iman_davenport = ((n - 1) * chi2) / float((n * (k - 1) - chi2))

    p_value = 1 - st.f.cdf(iman_davenport, k - 1, (k - 1) * (n - 1))

    return iman_davenport, p_value, rankings_avg, rankings_cmp


def nemenyi_multitest(pivots):
    """
    Performs a Nemenyi post-hoc test using the pivot quantities obtained by a ranking test.
    Tests the hypothesis that the ranking of each pair of groups are different.

    From: I. Rodríguez-Fdez, A. Canosa, M. Mucientes, A. Bugarín, STAC: a web platform for the comparison of
    algorithms using statistical tests, in: Proceedings of the 2015 IEEE International Conference on Fuzzy
    Systems (FUZZ-IEEE), 2015.

    https://github.com/citiususc/stac

    Parameters
    ----------
    pivots : list
        A list with the pivotal quantity for each group

    Returns
    ----------
    pvArray : array-like
        The associated p-values from the Z-distribution as a symmetrical 2D array

    References
    ----------
    Bonferroni-Dunn: O.J. Dunn, Multiple comparisons among means, Journal of the American Statistical
        Association 56 (1961) 52–64.
    """
    k = len(pivots)
    versus = list(it.combinations(range(k), 2))
    pvArray = np.zeros((k, k))
    m = int(k * (k - 1) / 2.)

    for vs in versus:
        z = abs(pivots[vs[0]] - pivots[vs[1]])
        pv = 2 * (1 - st.norm.cdf(abs(z)))
        pvArray[vs[0], vs[1]] = min(m * pv, 1)
        pvArray[vs[1], vs[0]] = min(m * pv, 1)

    return pvArray
