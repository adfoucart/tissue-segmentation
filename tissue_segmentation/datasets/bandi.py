"""
Code to read the images & annotations from:

Bándi, Péter. (2019).
Representative Sample Dataset for Resolution-Agnostic
Tissue Segmentation in Whole-Slide Histopathology Images (1.0.0) [Data set].
Zenodo. https://doi.org/10.5281/zenodo.3375528
"""
import os.path
from typing import Tuple

import numpy as np
import xml.etree.ElementTree as etree
from PIL import Image, ImageDraw

from .generic import GenericTissueDataset
from ..processing.vectors import FloatVector
from ..processing.slide import TiffWSI


class BandiDataset(GenericTissueDataset):
    _DEV_SET = [
        'breast_hne_00',
        'breast_lymph_node_hne_00',
        'tongue_ae1ae3_00',
        'tongue_hne_00',
        'tongue_ki67_00'
    ]
    _TEST_SET = [
        'brain_alcianblue_00',
        'cornea_grocott_00',
        'kidney_cab_00',
        'skin_perls_00',
        'uterus_vonkossa_00'
    ]

    def get_image_and_mask(self, name: str) -> Tuple[np.ndarray, np.ndarray]:
        """
        Images in the Bandi dataset are all stored in the base directory, with images stored as .tif & masks as .xml
        annotation files.
        :param name: name of the file (without extension)
        :return: Tuple with image and mask
        """
        wsi_path = os.path.join(self.basepath, f"{name}.tif")
        xml_path = os.path.join(self.basepath, f"{name}.xml")

        wsi = TiffWSI(wsi_path)
        target_dimensions = wsi.level_dimensions[self.target_level]
        target_downsample = wsi.level_downsamples[self.target_level]

        im = np.array(wsi.read_region((0, 0), self.target_level, target_dimensions))
        mask = get_bandi_annotation(xml_path, target_downsample, target_dimensions)
        return im, mask


def get_bandi_annotation(path: str,
                         target_downsample: int,
                         target_dimensions: Tuple[int, int],
                         with_smalltissue: bool = True):
    """
    Reads XML annotation file and build binary image mask.
    :param path: XML file
    :param target_downsample: downsample from the full-scale image
    :param target_dimensions: dimensions of the output mask
    :param with_smalltissue: include "small tissue" annotations in the mask
    :return: binary mask as an ndarray
    """
    tree = etree.parse(path)
    root = tree.getroot()

    groups = [group.attrib['Name'] for group in root.find('AnnotationGroups')]

    annos = {}
    for group in groups:
        annos[group] = []

    for ann in root.find('Annotations'):
        group = ann.attrib["PartOfGroup"]
        polygon = []
        for coord in ann.find('Coordinates'):
            polygon.append(FloatVector(x=float(coord.attrib['X']), y=float(coord.attrib['Y'])))
        annos[group].append(polygon)

    mask = Image.new('L', target_dimensions, 0)

    for tissue_region in annos.get('Tissue', []):
        points = [(np.round(p.x) / target_downsample, np.round(p.y) / target_downsample) for p in tissue_region]
        points += [points[0]]
        ImageDraw.Draw(mask).polygon(points, outline=5, fill=5)
    for tissue_region in annos.get('SmallTissue', []):
        points = [(np.round(p.x) / target_downsample, np.round(p.y) / target_downsample) for p in tissue_region]
        points += [points[0]]
        ImageDraw.Draw(mask).polygon(points, outline=4, fill=4)
    for tissue_region in annos.get('Artifacts', []):
        points = [(np.round(p.x) / target_downsample, np.round(p.y) / target_downsample) for p in tissue_region]
        points += [points[0]]
        ImageDraw.Draw(mask).polygon(points, outline=3, fill=3)
    for tissue_region in annos.get('Edge', []):
        points = [(np.round(p.x) / target_downsample, np.round(p.y) / target_downsample) for p in tissue_region]
        points += [points[0]]
        ImageDraw.Draw(mask).polygon(points, outline=2, fill=2)
    for tissue_region in annos.get('Background', []):
        points = [(np.round(p.x) / target_downsample, np.round(p.y) / target_downsample) for p in tissue_region]
        points += [points[0]]
        ImageDraw.Draw(mask).polygon(points, outline=1, fill=1)

    return np.array(mask) >= 4 if with_smalltissue else np.array(mask) >= 5
