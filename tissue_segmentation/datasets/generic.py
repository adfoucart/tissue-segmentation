"""
Code to read the images & annotations for Tissue Segmetation datasets
"""
from abc import ABC
from typing import Tuple, List

import numpy as np


class GenericTissueDataset(ABC):

    _DEV_SET: List[str] = []
    _TEST_SET: List[str] = []

    def __init__(self,
                 basepath: str,
                 train: bool = True,
                 target_level: int = 5):
        """
        :param basepath: Path to the directory where the dataset is stored.
        :param train: Boolean setting if the iterator will be on the training or on the test set.
        :param target_level: Level of the TIFF pyramid where the images will be extracted.
        """
        self.basepath = basepath
        self.train = train
        self.target_level = target_level

    def __iter__(self) -> Tuple[Tuple[np.ndarray, np.ndarray], str]:
        """
        Iterates on the images and masks in the train or test set.
        :return: (image, mask), name
        """
        if self.train:
            names = self._DEV_SET
        else:
            names = self._TEST_SET

        for name in names:
            yield self.get_image_and_mask(name), name

    def __len__(self):
        if self.train:
            return len(self._DEV_SET)
        else:
            return len(self._TEST_SET)

    def get(self, idx: int):
        if self.train:
            return self.get_image_and_mask(self._DEV_SET[idx]), self._DEV_SET[idx]
        return self.get_image_and_mask(self._TEST_SET[idx]), self._TEST_SET[idx]

    def get_image_and_mask(self, name: str) -> Tuple[np.ndarray, np.ndarray]:
        ...
