"""
Code to read the images & annotations for WSI Tissue Segmetation from:

TiGER challenge.
https://tiger.grand-challenge.org/
"""
import os.path
from typing import Tuple

import numpy as np

from .generic import GenericTissueDataset
from ..processing.slide import TiffWSI


class TigerDataset(GenericTissueDataset):
    _DEV_SET = [
        "111S",
        "124S",
        "128B",
        "130S",
        "140S",
        "141B",
        "144S",
        "156S",
        "166B",
        "170S",
        "173B",
        "177B",
        "183S",
        "187S",
        "195S",
        "197B",
        "200S",
        "213B",
        "229S",
        "235B",
        "238S",
        "239B",
        "241S",
        "249B",
        "TC_S01_P000002_C0001_B107",
        "TC_S01_P000009_C0001_B205",
        "TC_S01_P000012_C0001_B103",
        "TC_S01_P000017_C0001_B106",
        "TC_S01_P000023_C0001_B204",
        "TC_S01_P000031_C0001_B126",
        "TC_S01_P000034_C0001_B101",
        "TC_S01_P000035_C0001_B101",
        "TC_S01_P000040_C0001_B201",
        "TC_S01_P000044_C0001_B101",
        "TC_S01_P000048_C0001_B101",
        "TC_S01_P000051_C0001_B101",
        "TC_S01_P000056_C0001_B101",
        "TC_S01_P000066_C0001_B101",
        "TC_S01_P000069_C0001_B101",
        "TC_S01_P000072_C0001_B101",
        "TC_S01_P000076_C0001_B101",
        "TC_S01_P000090_C0001_B101",
        "TC_S01_P000098_C0001_B101",
        "TC_S01_P000102_C0001_B101",
        "TC_S01_P000109_C0001_B101",
        "TC_S01_P000111_C0001_B101",
        "TC_S01_P000117_C0001_B101",
        "TC_S01_P000118_C0001_B101",
        "TC_S01_P000121_C0001_B101",
        "TC_S01_P000125_C0001_B101",
        "TC_S01_P000126_C0001_B101",
        "TC_S01_P000133_C0001_B101",
        "TC_S01_P000140_C0001_B101",
        "TC_S01_P000144_C0001_B107",
        "TC_S01_P000150_C0001_B101",
        "TC_S01_P000153_C0001_B109",
        "TC_S01_P000162_C0001_B101",
        "TC_S01_P000170_C0001_B101",
        "TC_S01_P000173_C0001_B112",
        "TC_S01_P000177_C0001_B101",
        "TC_S01_P000180_C0001_B101",
        "TC_S01_P000185_C0001_B101"
    ]
    _TEST_SET = [
        "103S",
        "119S",
        "127B",
        "143S",
        "151S",
        "163S",
        "190S",
        "192B",
        "204S",
        "222B",
        "232S",
        "254S",
        "TC_S01_P000050_C0001_B101",
        "TC_S01_P000053_C0001_B101",
        "TC_S01_P000059_C0001_B101",
        "TC_S01_P000062_C0001_B102",
        "TC_S01_P000070_C0001_B103",
        "TC_S01_P000075_C0001_B101",
        "TC_S01_P000082_C0001_B101",
        "TC_S01_P000084_C0001_B101",
        "TC_S01_P000086_C0001_B101",
        "TC_S01_P000105_C0001_B201",
        "TC_S01_P000114_C0001_B101",
        "TC_S01_P000129_C0001_B101",
        "TC_S01_P000137_C0001_B101",
        "TC_S01_P000138_C0001_B101",
        "TC_S01_P000148_C0001_B101",
        "TC_S01_P000152_C0001_B101",
        "TC_S01_P000167_C0001_B101",
        "TC_S01_P000179_C0001_B101",
        "TC_S01_P000187_C0001_B101"
    ]

    def get_image_and_mask(self, name: str) -> Tuple[np.ndarray, np.ndarray]:
        """
        In the TiGER dataset, images are stored in the `images` subdirectory from the basepath, while masks are
        stored in the `tissue-masks` subdirectory, both as .tif files.
        :param name:
        :return:
        """
        wsi_path = os.path.join(self.basepath, f"images/{name}.tif")
        mask_path = os.path.join(self.basepath, f"tissue-masks/{name}_tissue.tif")

        wsi = TiffWSI(wsi_path)
        mask = TiffWSI(mask_path)

        im = np.array(wsi.read_region((0, 0),
                      self.target_level,
                      wsi.level_dimensions[self.target_level]))[..., :3]
        mask_ = np.array(mask.read_region((0, 0),
                         self.target_level,
                         mask.level_dimensions[self.target_level]))[..., 0] > 0
        return im, mask_
