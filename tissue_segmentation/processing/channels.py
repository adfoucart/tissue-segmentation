"""
Module containing methods to reduce RGB images to a single channel for further
processing.
"""
from __future__ import annotations

from functools import partial

import colour
import numpy as np
from skimage import img_as_ubyte
from skimage.color import rgb2gray, rgb2hsv
from skimage.filters.rank import entropy
from skimage.morphology import disk

rgb2avg = partial(np.mean, axis=2)


def grayscale(im: np.ndarray, invert: bool = True, as_ubyte: bool = False):
    """
    RGB to Grayscale
    :param im:
    :param invert:
    :param as_ubyte:
    :return:
    """
    gray = rgb2gray(im)
    if invert:
        gray = 1 - gray
    if as_ubyte:
        gray = img_as_ubyte(gray)
    return gray


def rgb_average(im: np.ndarray, invert: bool = True):
    """
    Average of the RGB channels
    :param im:
    :param invert:
    :return:
    """
    avg = rgb2avg(im)
    if invert:
        return avg.max() - avg
    return avg


def saturation(im: np.ndarray, invert: bool = False) -> np.ndarray:
    """
    Saturation channel from the HSV color space
    :param im:
    :param invert:
    :return:
    """
    if invert:
        return 1 - rgb2hsv(im)[..., 1]
    return rgb2hsv(im)[..., 1]


def colordistance(
        im: np.ndarray,
        brightness_q: float = 0.99,
        norm: bool = False,
        color_only: bool = False,
        mask_trivial: bool = True
    ) -> np.ndarray:
    """
    Color distance channel extractor, based on VALIS' tissue segmentation.
    https://github.com/MathOnco/valis/blob/main/valis/preprocessing.py
    :param im:
    :param brightness_q:
    :param norm:
    :param color_only:
    :param mask_trivial:
    :return:
    """
    eps = np.finfo("float").eps
    if np.issubdtype(im.dtype, np.integer) and im.max() > 1:
        rgb01 = im / 255.0
    else:
        rgb01 = im

    with colour.utilities.suppress_warnings(colour_usage_warnings=True):
        jab = colour.convert(rgb01 + eps, "sRGB", "CAM16UCS")

    if mask_trivial:
        brightest_thresh = np.quantile(jab[..., 0][im.min(axis=2) < 255], brightness_q)
    else:
        brightest_thresh = np.quantile(jab[..., 0], brightness_q)

    brightest_idx = np.where(jab[..., 0] >= brightest_thresh)

    if norm:
        jab = (jab - jab.min(axis=0).min(axis=0)) / (jab.max(axis=0).max(axis=0) - jab.min(axis=0).min(axis=0))

    brightest_pixels = jab[brightest_idx]
    bright_jab = brightest_pixels.mean(axis=0)
    if color_only:
        jab_d = np.sqrt(np.sum((jab[..., 1:] - bright_jab[1:]) ** 2, axis=2))
    else:
        jab_d = np.sqrt(np.sum((jab - bright_jab) ** 2, axis=2))

    return jab_d


def rgb2entropy(im: np.ndarray, radius: int = 10) -> np.ndarray:
    """
    Local entropy
    :param im:
    :param radius:
    :return:
    """
    return entropy(grayscale(im, invert=True, as_ubyte=True), disk(radius))
