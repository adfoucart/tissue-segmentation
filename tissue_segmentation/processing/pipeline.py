"""
Segmentation pipelines:

* Median + Otsu + [BinaryFillHole] (if BinaryFillHole -> compare with mask that is also filled!)
* Median + Learned threshold + [BinaryFillHole] (threshold found based on training set)
* FESI
* EntropyMasker
"""
from __future__ import annotations

from typing import List, Tuple

import numpy as np
from scipy.ndimage import binary_fill_holes, binary_opening, distance_transform_edt
from scipy.signal import argrelmin
from skimage.filters import laplace, gaussian, median
from skimage.filters.rank import entropy
from skimage.morphology import disk, flood_fill

from tissue_segmentation.processing.channels import grayscale


def entropy_masker(im: np.ndarray,
                   radius_pre: int,
                   radius_post: int,
                   min_thresh: float = 1.,
                   max_thresh: float = 4.) -> np.ndarray:
    """
    Reimplementation of EntropyMasker (Song et al., 2023 - https://doi.org/10.1038/s41598-023-29638-1).
    Code originally from https://github.com/CirculatoryHealth/EntropyMasker/blob/master/EntropyMaster/entropyMasker.py

    :param im:
    :param radius_pre:
    :param radius_post:
    :param min_thresh:
    :param max_thresh:
    :return:
    """
    im = grayscale(im, as_ubyte=True)
    entropy_image = entropy(im, disk(radius_pre))
    hist, bins = np.histogram(entropy_image.flatten(), 30)
    minindex = list(argrelmin(hist))[0]
    thresh = -1
    for i in range(len(minindex)):
        temp_thresh = bins[minindex[i]]
        if min_thresh < temp_thresh < max_thresh:
            thresh = temp_thresh
    if thresh < 0:
        raise ValueError(f"No minimum found between thresholds.")
    return entropy(im, disk(radius_post)) > thresh


def fesi(im: np.ndarray,
         median_ksize: int = 7,
         opening_ksize: int = 5,
         max_size_rel_threshold: float = 0.6,
         max_distance_abs_threshold: int = 500) -> np.ndarray:
    """
    Implementation of (Bug et al., 2015), adapted from
    https://github.com/NKI-AI/dlup/blob/main/dlup/background.py

    :param im:
    :param median_ksize:
    :param opening_ksize:
    :param max_size_rel_threshold:
    :param max_distance_abs_threshold:
    :return:
    """
    if len(im.shape) > 2:
        im = grayscale(im)
    log = _gaussian_of_laplacian(im)
    mask = log >= np.mean(log)
    mask = median(mask, np.ones((median_ksize, median_ksize)))
    mask = binary_opening(mask, np.ones((opening_ksize, opening_ksize)))
    mask = binary_fill_holes(mask)

    return _size_distance_filter(mask, max_size_rel_threshold, max_distance_abs_threshold)


def _gaussian_of_laplacian(im: np.ndarray,
                           laplacian_ksize: int = 3,
                           gaussian_sigma: float = 1.,
                           gaussian_truncate: float = 5.) -> np.ndarray:
    return gaussian(np.abs(laplace(im, ksize=laplacian_ksize)),
                    sigma=gaussian_sigma,
                    truncate=gaussian_truncate)


def _size_distance_filter(mask: np.ndarray,
                          max_size_rel_threshold: float,
                          max_distance_abs_threshold: int) -> np.ndarray:
    """
    As used in FESI
    From: https://github.com/NKI-AI/dlup/blob/main/dlup/background.py
    :param mask:
    :param max_size_rel_threshold:
    :param max_distance_abs_threshold:
    :return:
    """
    distance = distance_transform_edt(mask)

    final_mask = mask.copy().astype('uint8')
    maximal_distance = distance.max()
    global_max = distance.max()
    seeds: list = []
    while maximal_distance > 0:
        start = np.unravel_index(distance.argmax(), distance.shape)
        if (maximal_distance > max_size_rel_threshold * global_max) or _is_close(seeds, start[::-1],
                                                                                 max_distance_abs_threshold):
            flood_fill(final_mask, seed_point=start, new_value=200, in_place=True)
            seeds.append((start, maximal_distance))
        flood_fill(mask, seed_point=start, new_value=0, in_place=True)
        distance[mask == 0] = 0
        maximal_distance = distance.max()
    return final_mask == 200


def _is_close(_seeds: List[Tuple], _start: Tuple, max_dist: int = 500) -> bool:
    """
    Helper function for the FESI algorithms.
    From: https://github.com/NKI-AI/dlup/blob/main/dlup/background.py

    :param _seeds:
    :param _start:
    :param max_dist:
    :return:
    """
    _start = np.asarray(_start)
    for seed in _seeds:
        _seed = np.asarray(seed[0])
        if np.sqrt(((_start - _seed) ** 2).sum()) <= max_dist:
            return True
    return False
