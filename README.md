# Tissue segmentation

This code is linked to the following publication. Please cite it if you use it !

A. Foucart, A. Elskens, O. Debeir, C. Decaestecker  
Finding the best channel for tissue segmentation in whole-slide images  
*Proc. 19th International Symposium on Medical Information Processing and Analysis* (November 2023)  
https://doi.org/10.1109/SIPAIM56729.2023.10373416

Please adress all queries regarding this project to Adrien Foucart: Adrien.Foucart@ulb.be

## Data

Two sources of data are used in the study:

* The "Representative Sample Dataset for Resolution-Agnostic Tissue Segmentation" from Bándi et al. (https://zenodo.org/record/3375528)
* Part of the TiGER challenge dataset (https://tiger.grand-challenge.org/Data/). For the TiGER challenge, the `wsibulk/images` and `wsibulk/tissue-masks` folders are used.

## Code

The main files used for the submitted paper are the following. Note that for each `run_xxx.py` file, there is a 
corresponding `run_xxx.yml` configuration file to put the path to the datasets, and determine some parameters of the run.

* `run_descriptive_analysis.py`: computes the separability and the FPR v FNR analysis on the development set.
* `run_find_thresholds.py`: determines the best thresholds for each channel on the development set.
* `run_pipelines.py`: run the segmentation pipelines on the development or the test set.
* `generate_tables_and_figures.py`: from the results files saved by the `run_xxx.py` scripts, generate the tables and figures used in the paper.

The rest of the code in the `tissue-segmentation` module is structured as follows:

* `datasets` submodule: read the images and masks from the Tiger and Bandi datasets.
* `metrics` submodule: computes descriptive metrics and segmentation metrics.
* `processing` submodule: channel reduction steps, whole-slide slide I/O, reference algorithms and some helper functions.

The project uses the [`tifffile`](https://pypi.org/project/tifffile/) to read the whole-slide images and 
the [`colour-science`](https://github.com/colour-science/colour) library for CAM16UCS transforms.
