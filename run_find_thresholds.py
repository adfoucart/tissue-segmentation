"""
Find best thresholds for the different channels based on training set
"""
import json
import os
from dataclasses import dataclass

import numpy as np
import yaml
from matplotlib import pyplot as plt
from tqdm import tqdm

from tissue_segmentation.datasets.bandi import BandiDataset
from tissue_segmentation.datasets.generic import GenericTissueDataset
from tissue_segmentation.datasets.tiger import TigerDataset
from tissue_segmentation.metrics.segmentation import iou
from run_common import _CHANNELS, median_fn


@dataclass
class Config:
    bandi_dataset_path: str
    tiger_dataset_path: str
    output_path: str
    recompute_fixed_thresholds: bool
    recompute_relative_thresholds: bool
    with_median_filter: bool

    @classmethod
    def load(cls, config_path: str):
        _config = _load_config(config_path)
        return Config(
            bandi_dataset_path=_config["INPUTS"]["BANDI_DATASET"],
            tiger_dataset_path=_config["INPUTS"]["TIGER_DATASET"],
            output_path=_config["OUTPUTS"]["PATH"],
            recompute_fixed_thresholds=_config["RUN"]["RECOMPUTE_FIXED_THRESHOLDS"],
            recompute_relative_thresholds=_config["RUN"]["RECOMPUTE_RELATIVE_THRESHOLDS"],
            with_median_filter=_config["RUN"]["WITH_MEDIAN_FILTER"]
        )

    @property
    def prefix(self) -> str:
        if self.with_median_filter:
            return 'median_'
        return ''


def _load_config(config_path: str):
    with open(config_path, 'r') as fp:
        return yaml.safe_load(fp)


def _load_bandi(config) -> GenericTissueDataset:
    return BandiDataset(basepath=config.bandi_dataset_path,
                        train=True,
                        target_level=6)


def _load_tiger(config) -> GenericTissueDataset:
    return TigerDataset(basepath=config.tiger_dataset_path,
                        train=True,
                        target_level=5)


def main():
    config = Config.load('./run_find_thresholds.yml')
    datasets = [("bandi", _load_bandi(config)), ("tiger", _load_tiger(config))]

    thresholds = {}
    percentile_thresholds = {}
    for channel in _CHANNELS:
        thresholds[channel] = []
        percentile_thresholds[channel] = []

    if config.recompute_fixed_thresholds:
        for dsname, ds in datasets:
            pbar = tqdm(total=len(ds) * 5)
            for (im, mask), name in ds:
                mask_trivial = im.min(axis=2) < 255

                for channel, fn in _CHANNELS.items():
                    im_ = fn(im)
                    if config.with_median_filter:
                        im_ = median_fn(im_)
                    ts = []
                    p_ts = []
                    for t in np.linspace(im_.min(), im_.max(), 256):
                        pred = im_ >= t
                        m = iou(mask[mask_trivial], pred[mask_trivial])
                        ts.append((t, m))
                    for p in range(0, 101):
                        t = np.percentile(im_, p)
                        pred = im_ >= t
                        m = iou(mask[mask_trivial], pred[mask_trivial])
                        p_ts.append((p, m))
                    thresholds[channel].append(ts)
                    percentile_thresholds[channel].append(p_ts)
                    pbar.update(1)

        # save for reference
        with open(os.path.join(config.output_path, f"{config.prefix}all_thresholds.json"), "w") as fp:
            json.dump(thresholds, fp)
        with open(os.path.join(config.output_path, f"{config.prefix}all_percentile_thresholds.json"), "w") as fp:
            json.dump(percentile_thresholds, fp)
    else:
        with open(os.path.join(config.output_path, f"{config.prefix}all_thresholds.json"), "r") as fp:
            thresholds = json.load(fp)
        with open(os.path.join(config.output_path, f"{config.prefix}all_percentile_thresholds.json"), "r") as fp:
            percentile_thresholds = json.load(fp)

    best_t_for_channels = {}
    best_percentile_t_for_channels = {}
    for channel in _CHANNELS:
        all_percentile_ts_for_channel = np.array(percentile_thresholds[channel])
        mean_iou_per_percentile = np.mean(all_percentile_ts_for_channel[..., 1], axis=0)
        best_percentile_t_for_channels[channel] = int(np.argmax(mean_iou_per_percentile))

        all_ts_for_channel = np.array(thresholds[channel])
        # re-sample on common scale
        t_min = all_ts_for_channel[..., 0].min()
        t_max = all_ts_for_channel[..., 0].max()
        potential_ts = np.linspace(t_min, t_max, 300)
        sum_iou_for_t = np.zeros_like(potential_ts)
        n_iou_for_t = np.zeros_like(potential_ts)
        for idt, t in enumerate(potential_ts):
            for all_ts_for_image in all_ts_for_channel:
                # find closest threshold
                diff = np.abs(all_ts_for_image[..., 0]-t)
                closest_t = all_ts_for_image[np.argmin(diff), 0]
                i_diff = np.abs(potential_ts-closest_t)
                i_closest = potential_ts[np.argmin(i_diff)]
                if np.isclose(t, i_closest):
                    sum_iou_for_t[idt] += all_ts_for_image[np.argmin(diff), 1]
                    n_iou_for_t[idt] += 1
        avg_iou_for_t = sum_iou_for_t/np.maximum(n_iou_for_t, 1)
        best_t_for_channels[channel] = potential_ts[np.argmax(avg_iou_for_t)]

    with open(os.path.join(config.output_path, f"{config.prefix}best_thresholds_per_channel.json"), "w") as fp:
        json.dump(best_t_for_channels, fp)
    with open(os.path.join(config.output_path, f"{config.prefix}best_percentile_thresholds_per_channel.json"), "w") as fp:
        json.dump(best_percentile_t_for_channels, fp)

    for channel, best_t in best_t_for_channels.items():
        print(channel, best_t)

    for channel, best_t in best_percentile_t_for_channels.items():
        print(channel, f"percentile={best_t}")

    # Check the IoUs that we get with the learned threshold on the whole training set
    ious_for_best_t_per_channel = {}
    for c, best_t in best_t_for_channels.items():
        all_ts_for_channel = np.array(thresholds[c])
        d_from_t = np.abs(all_ts_for_channel[..., 0] - best_t)
        argts = np.argmin(d_from_t, axis=1)
        ious_for_best_t = []
        for ious, argt in zip(all_ts_for_channel[..., 1], argts):
            ious_for_best_t.append(ious[argt])
        ious_for_best_t_per_channel[c] = ious_for_best_t

    plt.figure(figsize=(6, 6))
    plt.boxplot(ious_for_best_t_per_channel.values(), labels=ious_for_best_t_per_channel.keys())
    plt.ylabel('IoU')
    plt.savefig(os.path.join(config.output_path, f"figs_descriptive/{config.prefix}iou_at_learned_t_boxplot.png"))

    # Check the IoUs that we get with the learned percentile threshold on the whole training set
    ious_for_best_t_per_channel = {}
    for c, best_t in best_percentile_t_for_channels.items():
        all_percentile_ts_for_channel = np.array(percentile_thresholds[c])
        ious_for_best_t_per_channel[c] = all_percentile_ts_for_channel[:, int(best_t), 1]

    plt.figure(figsize=(6, 6))
    plt.boxplot(ious_for_best_t_per_channel.values(), labels=ious_for_best_t_per_channel.keys())
    plt.ylabel('IoU')
    plt.savefig(os.path.join(config.output_path, f"figs_descriptive/{config.prefix}iou_at_learned_percentile_t_boxplot.png"))


if __name__ == "__main__":
    main()
