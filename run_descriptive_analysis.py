"""
Using:

    5 WSIs from Bandi dataset (H&E + IHC, Radboud)
    52 WSIs from TiGER dataset (H&E, Radboud & Border)

    Load WSI @ ~15µm/px resolution
    Remove "trivial" area (pure white in all Radboud WSIs)
    Compute channel transformations (Grayscale, RGBAvg, Saturation, Colorfullness, LocalEntropy)
    For each channel & WSI: compute separability & FPR@0:10 FNR
"""
import csv
import os
from dataclasses import dataclass
from typing import Dict

import numpy as np
import yaml
from matplotlib import pyplot as plt
from scipy.stats import rankdata
from tqdm import tqdm

from tissue_segmentation.datasets.bandi import BandiDataset
from tissue_segmentation.datasets.generic import GenericTissueDataset
from tissue_segmentation.datasets.tiger import TigerDataset
from tissue_segmentation.metrics.descriptive import get_descriptive_metrics
from run_common import _CHANNELS, median_fn


@dataclass
class Config:
    bandi_dataset_path: str
    tiger_dataset_path: str
    output_path: str
    recompute_metrics_bandi: bool
    recompute_metrics_tiger: bool
    with_median_filter: bool

    @classmethod
    def load(cls, config_path: str):
        _config = _load_config(config_path)
        return Config(
            bandi_dataset_path=_config["INPUTS"]["BANDI_DATASET"],
            tiger_dataset_path=_config["INPUTS"]["TIGER_DATASET"],
            output_path=_config["OUTPUTS"]["PATH"],
            recompute_metrics_bandi=_config["RUN"]["RECOMPUTE_METRICS_BANDI"],
            recompute_metrics_tiger=_config["RUN"]["RECOMPUTE_METRICS_TIGER"],
            with_median_filter=_config["RUN"]["WITH_MEDIAN_FILTER"]
        )


def _load_config(config_path: str):
    with open(config_path, 'r') as fp:
        return yaml.safe_load(fp)


def _load_bandi(config) -> GenericTissueDataset:
    return BandiDataset(basepath=config.bandi_dataset_path,
                        train=True,
                        target_level=6)


def _load_tiger(config) -> GenericTissueDataset:
    return TigerDataset(basepath=config.tiger_dataset_path,
                        train=True,
                        target_level=5)


def save_results(dsname: str, results: Dict, output_path: str, prefix: str = ''):
    with open(os.path.join(output_path, f"{prefix}{dsname}_descriptive.csv"), "w", newline='', encoding='utf-8') as fp:
        writer = csv.writer(fp)
        writer.writerow(["Slide", "Channel", "Sep"] + [f"FPR@{i}" for i in range(0, 11)])
        for name in results:
            for channel in results[name]:
                sep, fprs = results[name][channel]
                writer.writerow([name, channel, sep] + list(fprs))


def load_results(output_path: str, prefix: str = ''):
    separability_per_channel = {}
    fprs_per_channel = {}

    for channel in _CHANNELS:
        separability_per_channel[channel] = []
        fprs_per_channel[channel] = []

    for dsname in ['bandi', 'tiger']:
        with open(os.path.join(output_path, f"{prefix}{dsname}_descriptive.csv"),
                  "r",
                  newline='',
                  encoding='utf-8') as fp:
            reader = csv.reader(fp)
            for row in reader:
                if len(row) <= 1 or row[1] not in _CHANNELS:
                    continue
                separability_per_channel[row[1]].append(float(row[2]))
                fprs_per_channel[row[1]].append([float(v) for v in row[3:]])

    return separability_per_channel, fprs_per_channel


def compute_and_save_statistics(output_path: str, prefix: str):
    print("Computing statistics...")
    # Separability analysis
    separability_per_channel, fprs_per_channel = load_results(output_path, prefix)

    all_seps = np.zeros((len(separability_per_channel), len(separability_per_channel['Grayscale'])))
    for idc, channel in enumerate(separability_per_channel):
        all_seps[idc] = np.array(separability_per_channel[channel])

    ranks_sep = np.zeros_like(all_seps)
    for i in range(all_seps.shape[1]):
        ranks_sep[:, i] = 6 - rankdata(all_seps[:, i])

    plt.figure(figsize=(8, 4))
    plt.boxplot(all_seps.T, labels=list(separability_per_channel.keys()))
    plt.ylabel('Separability')
    plt.savefig(os.path.join(output_path, f"figs_descriptive/{prefix}separability_boxplot.png"))

    with open(os.path.join(output_path, f"{prefix}separability_ranks.txt"), "w") as fp:
        fp.write("Average rank:\n")
        for idc, channel in enumerate(separability_per_channel):
            fp.write(f"{channel:10}\t{np.mean(ranks_sep[idc]):.2f}\n")

    # FPRs analysis
    all_fprs = np.zeros((len(fprs_per_channel), len(fprs_per_channel['Grayscale']), 11))

    for idc, channel in enumerate(fprs_per_channel):
        all_fprs[idc] = np.array(fprs_per_channel[channel])
    for p in range(11):
        plt.figure(figsize=(8, 4))
        plt.boxplot(all_fprs[..., p].T, labels=list(fprs_per_channel.keys()))
        plt.ylabel(f"FPR@{p}FNR")
        plt.savefig(os.path.join(output_path, f"figs_descriptive/{prefix}fprs_{p}fnr_boxplot.png"))

    ranks_fprs = np.zeros_like(all_fprs)
    for i in range(all_fprs.shape[1]):
        for p in range(all_fprs.shape[2]):
            ranks_fprs[:, i, p] = rankdata(all_fprs[:, i, p])

    with open(os.path.join(output_path, f"{prefix}fprs_ranks.txt"), "w") as fp:
        fp.write(f"Channel     \t" + "\t".join([f"Rank@{p}" for p in range(11)]) + "\n")
        fp.write("---" * 10 + "\n")
        for idc, channel in enumerate(fprs_per_channel):
            s = f"{channel:10}\t"
            for p in range(11):
                ranksp = ranks_fprs[:, :, p]
                s += f"{np.mean(ranksp[idc]):.2f}\t"
            fp.write(s + "\n")

    mean_fprs = all_fprs.mean(axis=1)

    plt.figure(figsize=(6, 6))
    plt.plot(mean_fprs.T, label=list(fprs_per_channel.keys()))
    plt.legend()
    plt.xlabel('FNR [%]')
    plt.ylabel('Avg FPR')
    plt.savefig(os.path.join(output_path, f"figs_descriptive/{prefix}fprs_mean_plot.png"))

    with open(os.path.join(output_path, f"{prefix}fprs_mean.txt"), "w") as fp:
        fp.write(f"Channel     \t" + "\t".join([f"Mean@{p}" for p in range(11)]) + "\n")
        fp.write("---" * 10 + "\n")
        for idc, channel in enumerate(fprs_per_channel):
            s = f"{channel:10}\t"
            for p in range(11):
                s += f"{np.mean(all_fprs[idc, :, p]):.2f}\t"
            fp.write(s + "\n")

    median_fprs = np.median(all_fprs, axis=1)

    plt.figure(figsize=(6, 6))
    plt.plot(median_fprs.T, label=list(fprs_per_channel.keys()))
    plt.legend()
    plt.xlabel('FNR [%]')
    plt.ylabel('Median FPR')
    plt.savefig(os.path.join(output_path, f"figs_descriptive/{prefix}fprs_median_plot.png"))

    with open(os.path.join(output_path, f"{prefix}fprs_median.txt"), "w") as fp:
        fp.write(f"Channel     \t" + "\t".join([f"Mean@{p}" for p in range(11)]) + "\n")
        fp.write("---" * 10 + "\n")
        for idc, channel in enumerate(fprs_per_channel):
            s = f"{channel:10}\t"
            for p in range(11):
                s += f"{np.median(all_fprs[idc, :, p]):.2f}\t"
            fp.write(s + "\n")

    for idc, channel in enumerate(fprs_per_channel):
        plt.figure(figsize=(6, 6))
        for p in range(11):
            plt.scatter(np.ones((all_fprs.shape[1])) * p, all_fprs[idc, :, p], color='b', marker='+')
        plt.title(channel)
        plt.xlabel('FNR [%]')
        plt.ylabel('FPR')
        plt.savefig(os.path.join(output_path, f"figs_descriptive/{prefix}fprs_{channel}_scatter.png"))


def main():
    config = Config.load('./run_descriptive_analysis.yml')
    datasets = []
    n_ticks = 0
    prefix = ""
    if config.with_median_filter:
        prefix = "median_"
    if config.recompute_metrics_bandi:
        ds = _load_bandi(config)
        datasets.append(("bandi", ds))
        n_ticks += len(ds)
    if config.recompute_metrics_tiger:
        ds = _load_tiger(config)
        datasets.append(("tiger", ds))
        n_ticks += len(ds)

    if n_ticks > 0:
        print("Computing descriptive analysis")
        pbar = tqdm(total=n_ticks*5)
        for dsname, ds in datasets:
            results = {}
            for (im, mask), name in ds:
                mask_trivial = im.min(axis=2) == 255

                results[name] = {}
                for channel, fn in _CHANNELS.items():
                    im_ = fn(im)
                    if config.with_median_filter:
                        im_ = median_fn(im_)
                    sep, fprs = get_descriptive_metrics(im_, mask, mask_trivial)
                    results[name][channel] = (sep, fprs)
                    pbar.update(1)

            save_results(dsname, results, config.output_path, prefix)

    compute_and_save_statistics(config.output_path, prefix)


if __name__ == "__main__":
    main()
