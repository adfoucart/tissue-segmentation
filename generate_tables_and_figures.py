"""
Generate the tables and figures for the SIPAIM submission (plus some additional tables and figures)
"""
import logging
import os
import csv
import json

from scipy.stats import rankdata
import numpy as np
from matplotlib import pyplot as plt
from skimage.io import imread

from tissue_segmentation.metrics import friedman_test, nemenyi_multitest, interquartiles_str
from tissue_segmentation.processing.channels import grayscale, saturation, colordistance, rgb2entropy


PATH_TO_RESULTS = r".\results"
CHANNELS = ["Grayscale", "Saturation", "ColorDist", "LocalEntropy"]

RESULTS_DISPLAY = {
    "EntropyMasker": "EntropyMasker",
    "FESI": "FESI",
    "LT-Grayscale": "Grayscale (t=0.026)",
    "LT-Saturation": "Saturation (t=0.029)",
    "LT-ColorDist": "ColorDist (t=0.030)",
    "LT-LocalEntropy": "LocalEntropy (t=3.85)",
    "MOtsu-Grayscale": "Grayscale (Otsu)",
    "MOtsu-Saturation": "Saturation (Otsu)",
    "MOtsu-ColorDist": "ColorDist (Otsu)",
    "Otsu-LocalEntropy": "LocalEntropy (Otsu)"
}


def save_boxplot_and_average_rank(with_median: bool = True):
    separability_per_channel = {}
    for channel in CHANNELS:
        separability_per_channel[channel] = []

    str_out = ""

    prefix = ""
    if with_median:
        prefix = "median_"

    for ds in ['bandi', 'tiger']:
        with open(os.path.join(PATH_TO_RESULTS, f"{prefix}{ds}_descriptive.csv"), "r", newline='',
                  encoding='utf-8') as fp:
            reader = csv.reader(fp)
            for row in reader:
                if len(row) <= 1 or row[1] not in separability_per_channel:
                    continue
                separability_per_channel[row[1]].append(float(row[2]))

    all_seps = np.zeros((len(separability_per_channel), len(separability_per_channel['Grayscale'])))

    for idc, channel in enumerate(separability_per_channel):
        all_seps[idc] = np.array(separability_per_channel[channel])

    ranks = np.zeros_like(all_seps)
    for i in range(all_seps.shape[1]):
        ranks[:, i] = len(separability_per_channel) + 1 - rankdata(all_seps[:, i])

    plt.figure(figsize=(8, 4))
    plt.boxplot(all_seps.T, labels=list(separability_per_channel.keys()))
    plt.ylabel('Separability')
    plt.savefig(f"./{prefix}sep_per_channel.png")

    str_out += "Average rank:\n"
    for idc, channel in enumerate(separability_per_channel):
        str_out += f"{channel:10}\t{np.mean(ranks[idc]):.2f}\n"

    str_out += "Interquartiles:\n"
    for idc, channel in enumerate(separability_per_channel):
        str_out += f"{channel:10}\t{interquartiles_str(all_seps[idc])}\n"

    _, pval, rankings, pivots = friedman_test(*separability_per_channel.values())
    str_out += f"Friedman p-value = {pval}\n"

    best_rank = np.argmax(rankings)
    channels = list(separability_per_channel.keys())
    str_out += f"Best = {channels[best_rank]}\n"

    pvarr = nemenyi_multitest(pivots)

    str_out += "Statistically different (p<0.0.5) from:\n"
    for i, pv in enumerate(pvarr[best_rank]):
        if i == best_rank:
            continue
        if pv < 0.05:
            str_out += f"{channels[i]} ({pv=})\n"

    with open(f'./{prefix}sep_per_channel.txt', 'w') as fp:
        fp.write(str_out)


def save_fpr_fnr_plot(with_median: bool = True):
    fprs_per_channel = {}
    for channel in CHANNELS:
        fprs_per_channel[channel] = []

    _markers = ['k--', 'r-', 'b-.', 'c:']
    markers = {}
    for m, channel in zip(_markers, CHANNELS):
        markers[channel] = m

    prefix = ""
    if with_median:
        prefix = "median_"

    for ds in ['bandi', 'tiger']:
        with open(os.path.join(PATH_TO_RESULTS, f"{prefix}{ds}_descriptive.csv"), "r", newline='',
                  encoding='utf-8') as fp:
            reader = csv.reader(fp)
            for row in reader:
                if len(row) <= 1 or row[1] not in fprs_per_channel:
                    continue
                fprs_per_channel[row[1]].append([float(v) for v in row[3:]])

    all_fprs = np.zeros((len(fprs_per_channel), len(fprs_per_channel['Grayscale']), 11))

    for idc, channel in enumerate(fprs_per_channel):
        all_fprs[idc] = np.array(fprs_per_channel[channel])

    mean_fprs = all_fprs.mean(axis=1) * 100

    plt.figure(figsize=(6, 6))
    for idc, channel in enumerate(markers):
        plt.plot(mean_fprs[idc, :], markers[channel], label=channel)
    plt.legend()
    plt.xlabel('FNR [%]')
    plt.ylabel('Avg FPR [%]')
    plt.ylim([0, 100])
    plt.savefig(f"./{prefix}fpr_fnr.png")


def get_results_table():
    results_baselines = {
        "EntropyMasker": "results_entropy_masker.json",
        "FESI": "results_fesi_Grayscale.json",
    }
    results_channels = {
        "LT": "results_fixed_pre_post.json",
        "Otsu": "results_otsu_pre_post.json",
        "MOtsu": "results_multiotsu_pre_post.json"
    }

    ious = {}

    for name, f in results_baselines.items():
        ious[name] = []
        with open(os.path.join(PATH_TO_RESULTS, f), "r") as fp:
            results = json.load(fp)
        for res_, res_filled in results:
            ious[name].append(res_filled['iou'])

    thresholds = {}

    for name, f in results_channels.items():
        for channel in CHANNELS:
            name_ = f"{name}-{channel}"
            ious[name_] = []
            thresholds[name_] = []

        with open(os.path.join(PATH_TO_RESULTS, f), "r") as fp:
            results = json.load(fp)

        for channel in CHANNELS:
            name_ = f"{name}-{channel}"
            for t, res_, res_filled in results[channel]:
                ious[name_].append(res_filled['iou'])
                thresholds[name_].append(t)

    return ious, thresholds


def save_representative_image():
    im = imread('./samples/TC_S01_P000148_C0001_B101.bmp')[500:1500, 3000:3500]
    mask = imread('./samples/TC_S01_P000148_C0001_B101_tissue.bmp')[500:1500, 3000:3500]
    fesi = imread('./samples/fesi_TC_S01_P000148_C0001_B101.png')[500:1500, 3000:3500]
    em = imread('./samples/em_TC_S01_P000148_C0001_B101.png')[500:1500, 3000:3500]
    ft = {
        "Grayscale": imread('./samples/ft_Grayscale_TC_S01_P000148_C0001_B101.png')[500:1500, 3000:3500],
        "Saturation": imread('./samples/ft_Saturation_TC_S01_P000148_C0001_B101.png')[500:1500, 3000:3500],
        "ColorDist": imread('./samples/ft_ColorDist_TC_S01_P000148_C0001_B101.png')[500:1500, 3000:3500],
        "LocalEntropy": imread('./samples/ft_LocalEntropy_TC_S01_P000148_C0001_B101.png')[500:1500, 3000:3500]
    }
    otsu = {
        "Grayscale": imread('./samples/otsu_Grayscale_TC_S01_P000148_C0001_B101.png')[500:1500, 3000:3500],
        "Saturation": imread('./samples/otsu_Saturation_TC_S01_P000148_C0001_B101.png')[500:1500, 3000:3500],
        "ColorDist": imread('./samples/otsu_ColorDist_TC_S01_P000148_C0001_B101.png')[500:1500, 3000:3500],
        "LocalEntropy": imread('./samples/otsu_LocalEntropy_TC_S01_P000148_C0001_B101.png')[500:1500, 3000:3500]
    }

    channel_fns = {
        "Grayscale": grayscale,
        "Saturation": saturation,
        "ColorDist": colordistance,
        "LocalEntropy": rgb2entropy
    }

    plt.figure(figsize=(15, 7))
    plt.subplot(2, 6, 1)
    plt.imshow(im)
    plt.contour(mask)
    plt.title("Annotation")
    plt.subplot(2, 6, 2)
    plt.imshow(im)
    plt.contour(fesi)
    plt.title("FESI")
    plt.subplot(2, 6, 8)
    plt.imshow(im)
    plt.contour(em)
    plt.title("EntropyMasker")
    im_ = {}
    for i, (channel, fn) in enumerate(channel_fns.items()):
        im_[channel] = fn(im)
        plt.subplot(2, 6, 3 + i)
        plt.imshow(im_[channel], cmap=plt.cm.gray)
        plt.contour(ft[channel])
        plt.title(f"FT-{channel}")
    for i, (channel, fn) in enumerate(channel_fns.items()):
        plt.subplot(2, 6, 9 + i)
        plt.imshow(im_[channel], cmap=plt.cm.gray)
        plt.contour(otsu[channel])
        plt.title(f"Otsu-{channel}")
    plt.savefig("./representative_image.png")


def main():
    logging.info("Generating descriptive analysis boxplots.")
    save_boxplot_and_average_rank(with_median=True)
    save_boxplot_and_average_rank(with_median=False)

    logging.info("Generating FPR v FNR plots.")
    save_fpr_fnr_plot(with_median=True)
    save_fpr_fnr_plot(with_median=False)

    logging.info("Loading IoUs")
    ious, thresholds = get_results_table()

    results_to_test = []

    for name, label in RESULTS_DISPLAY.items():
        results_to_test.append(ious[name])

    logging.info("Statistical analysis")
    _, pval, rankings, pivots = friedman_test(*results_to_test)

    best_rank = np.argmax(rankings)
    algorithms = list(RESULTS_DISPLAY.keys())
    pvarr = nemenyi_multitest(pivots)

    str_out = "ALGORITHM\t\tIoU InterQuartiles\n"

    for name, dname in RESULTS_DISPLAY.items():
        str_out += f"{dname:20}\t{interquartiles_str(ious[name])}\n"

    str_out += f"Friedman p-val = {pval}\n"
    str_out += f"Best = {RESULTS_DISPLAY[algorithms[best_rank]]}\n"
    str_out += "Statistically not significantly different (p>0.0.5) from:\n"
    for i, pv in enumerate(pvarr[best_rank]):
        if i == best_rank:
            continue
        if pv > 0.05:
            str_out += f"{RESULTS_DISPLAY[algorithms[i]]} ({pv=})\n"

    with open('./results_pipelines.txt', 'w') as fp:
        fp.write(str_out)

    logging.info("Representative image")
    save_representative_image()


if __name__ == "__main__":
    main()
