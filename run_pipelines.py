"""
Run baseline (FESI & EntropyMasker) + generic pipeline w/ diff channels
"""
import json
import os
from dataclasses import dataclass
from functools import partial
from typing import Tuple, List, Callable, Dict, Optional

import numpy as np
import yaml
from scipy.ndimage import binary_fill_holes, binary_opening
from skimage import img_as_ubyte
from skimage.filters.thresholding import threshold_otsu, threshold_multiotsu
from skimage.io import imsave
from skimage.morphology import disk

from tissue_segmentation.datasets.bandi import BandiDataset
from tissue_segmentation.datasets.generic import GenericTissueDataset
from tissue_segmentation.datasets.tiger import TigerDataset
from run_common import _CHANNELS, median_fn
from tissue_segmentation.metrics.segmentation import get_all_metrics
from tissue_segmentation.processing.pipeline import fesi, entropy_masker

ThresholdFunction = Callable[[np.ndarray, bool, bool], Tuple[np.ndarray, float]]


@dataclass
class Config:
    bandi_dataset_path: str
    tiger_dataset_path: str
    output_path: str
    compute_on_dev_set: bool
    compute_on_test_set: bool
    with_preprocessing: bool
    with_postprocessing: bool
    run_fesi: bool
    run_entropymasker: bool
    run_channels_otsu: bool
    run_channels_multiotsu: bool
    run_channels_fixed: bool
    run_channels_percentile: bool
    run_single_image: str | bool

    @classmethod
    def load(cls, config_path: str):
        _config = _load_config(config_path)
        return Config(
            bandi_dataset_path=_config["INPUTS"]["BANDI_DATASET"],
            tiger_dataset_path=_config["INPUTS"]["TIGER_DATASET"],
            output_path=_config["OUTPUTS"]["PATH"],
            compute_on_dev_set=_config["RUN"]["COMPUTE_ON_DEV_SET"],
            compute_on_test_set=_config["RUN"]["COMPUTE_ON_TEST_SET"],
            with_preprocessing=_config["RUN"]["WITH_PREPROCESSING"],
            with_postprocessing=_config["RUN"]["WITH_POSTPROCESSING"],
            run_fesi=_config["RUN"]["FESI"],
            run_entropymasker=_config["RUN"]["ENTROPYMASKER"],
            run_channels_otsu=_config["RUN"]["RUN_CHANNELS_OTSU"],
            run_channels_multiotsu=_config["RUN"]["RUN_CHANNELS_MULTIOTSU"],
            run_channels_fixed=_config["RUN"]["RUN_CHANNELS_FIXED"],
            run_channels_percentile=_config["RUN"]["RUN_CHANNELS_PERCENTILE"],
            run_single_image=_config["RUN"]["SINGLE_IMAGE"]
        )


def _load_config(config_path: str):
    with open(config_path, 'r') as fp:
        return yaml.safe_load(fp)


def _load_bandi(config, train: bool) -> GenericTissueDataset:
    return BandiDataset(basepath=config.bandi_dataset_path,
                        train=train,
                        target_level=6)


def _load_tiger(config, train: bool) -> GenericTissueDataset:
    return TigerDataset(basepath=config.tiger_dataset_path,
                        train=train,
                        target_level=5)


def _postprocess(im: np.ndarray) -> np.ndarray:
    im = binary_fill_holes(im)
    return binary_opening(im, disk(5))


def _threshold_on_image(im: np.ndarray,
                        with_postprocessing: bool,
                        with_preprocessing: bool,
                        threshold_fn: Callable[[np.ndarray], float]) -> Tuple[np.ndarray, float]:
    if with_preprocessing:
        im = median_fn(im)
    t = threshold_fn(im)
    pred = im > t
    if with_postprocessing:
        pred = _postprocess(pred)
    return pred, t


def otsu_on_image(im: np.ndarray, with_postprocessing: bool = False, with_preprocessing: bool = False):
    return _threshold_on_image(im, with_postprocessing, with_preprocessing, threshold_otsu)


def multiotsu_on_image(im: np.ndarray, with_postprocessing: bool = False, with_preprocessing: bool = False):
    def threshold_fn(im_: np.ndarray) -> float:
        return threshold_multiotsu(im_, classes=3)[0]

    return _threshold_on_image(im, with_postprocessing, with_preprocessing, threshold_fn)


def fixed_t_on_image(im: np.ndarray, t: float, with_postprocessing: bool = False, with_preprocessing: bool = False):
    def threshold_fn(im_: np.ndarray) -> float:
        return t
    return _threshold_on_image(im, with_postprocessing, with_preprocessing, threshold_fn)


def percentile_t_on_image(im: np.ndarray, p: float, with_postprocessing: bool = False, with_preprocessing: bool = False):
    def threshold_fn(im_: np.ndarray) -> float:
        return np.percentile(im, p)
    return _threshold_on_image(im, with_postprocessing, with_preprocessing, threshold_fn)


def _mask_preprocess(im: np.ndarray, mask: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    mask_trivial = im.min(axis=2) == 255
    mask_filled = binary_fill_holes(mask)
    mask[mask_trivial] = 0
    mask_filled[mask_trivial] = 0

    return mask, mask_filled, mask_trivial


def thresholding_pipeline(datasets: List[Tuple[str, GenericTissueDataset]],
                          config: Config,
                          threshold_fn: ThresholdFunction | Dict[str, ThresholdFunction],
                          prefix: str = ""):
    results = {}
    for channel in _CHANNELS:
        results[channel] = []

    suffix = ""
    if config.with_preprocessing:
        suffix += "_pre"
    if config.with_postprocessing:
        suffix += "_post"

    for dsname, ds in datasets:
        for (im, mask), name in ds:
            if config.run_single_image is not False and name != config.run_single_image:
                print(f"Skipping {name}")
                continue
            mask, mask_filled, mask_trivial = _mask_preprocess(im, mask)

            for channel, fn in _CHANNELS.items():
                im_ = fn(im)
                if type(threshold_fn) is dict:
                    pred, t = threshold_fn[channel](im_, config.with_preprocessing, config.with_postprocessing)
                else:
                    pred, t = threshold_fn(im_, config.with_preprocessing, config.with_postprocessing)
                pred[mask_trivial] = 0
                if config.run_single_image is not False:
                    imsave(os.path.join(config.output_path, f"img_pred/{prefix}_{channel}_{name}{suffix}.png"),
                           img_as_ubyte(pred))
                m = get_all_metrics(mask, pred)
                mf = get_all_metrics(mask_filled, pred)
                print(name, m)
                results[channel].append((t, m, mf))

    return results


def fixed_thresholding_pipeline(datasets: List[Tuple[str, GenericTissueDataset]],
                                config: Config):

    # load best thresholds
    prefix = ""
    if config.with_preprocessing:
        prefix = "median_"
    with open(os.path.join(config.output_path, f"{prefix}best_thresholds_per_channel.json"), "r") as fp:
        best_t_for_channels = json.load(fp)

    threshold_fn = {}
    for channel in _CHANNELS:
        threshold_fn[channel] = partial(fixed_t_on_image, t=best_t_for_channels[channel])

    return thresholding_pipeline(datasets, config, threshold_fn, "fixed")


def percentile_thresholding_pipeline(datasets: List[Tuple[str, GenericTissueDataset]],
                                     config: Config):
    results = {}

    for channel in _CHANNELS:
        results[channel] = []

    # load best thresholds
    prefix = ""
    if config.with_preprocessing:
        prefix = "median_"
    with open(os.path.join(config.output_path, f"{prefix}best_percentile_thresholds_per_channel.json"), "r") as fp:
        best_p_for_channels = json.load(fp)

    threshold_fn = {}
    for channel in _CHANNELS:
        threshold_fn[channel] = partial(percentile_t_on_image, p=best_p_for_channels[channel])

    return thresholding_pipeline(datasets, config, threshold_fn, "percentile")


def base_pipeline(datasets: List[Tuple[str, GenericTissueDataset]],
                  config: Config,
                  processing_fn: Callable[[np.ndarray], np.ndarray],
                  prefix: str = "base_pipeline"):
    results = []

    for dsname, ds in datasets:
        for (im, mask), name in ds:
            if config.run_single_image is not False and name != config.run_single_image:
                print(f"Skipping {name}")
                continue
            mask, mask_filled, mask_trivial = _mask_preprocess(im, mask)

            pred = processing_fn(im)
            pred[mask_trivial] = 0
            if config.run_single_image is not False:
                imsave(os.path.join(config.output_path, f"img_pred/{prefix}_{name}.png"), img_as_ubyte(pred))
            m = get_all_metrics(mask, pred)
            mf = get_all_metrics(mask_filled, pred)
            print(name, m)
            results.append((m, mf))

    return results


def save_results(results, config: Config, method_name: str):

    suffix = ""
    if config.with_preprocessing:
        suffix += "_pre"
    if config.with_postprocessing:
        suffix += "_post"

    output_name = f"{method_name}_results{suffix}.json"

    with open(os.path.join(config.output_path, output_name), "w") as fp:
        json.dump(results, fp)


def run_on_datasets(config: Config, train: bool):
    if not os.path.exists(config.output_path):
        raise NotADirectoryError(f"{config.output_path} does not exist or is not a directory. "
                                 f"Please set OUTPUT_PATH to a valid value in the run_pipelines.yml file.")
    if not os.path.exists(os.path.join(config.output_path, "img_pred")):
        os.mkdir(os.path.join(config.output_path, "img_pred"))

    datasets = [("bandi", _load_bandi(config, train)), ("tiger", _load_tiger(config, train))]
    tstr = "test"
    if train:
        tstr = "dev"

    if config.run_channels_otsu:
        save_results(thresholding_pipeline(datasets, config, otsu_on_image, "otsu"),
                     config,
                     f"{tstr}_otsu")

    if config.run_channels_multiotsu:
        save_results(thresholding_pipeline(datasets, config, multiotsu_on_image, "multiotsu"),
                     config,
                     f"{tstr}_multiotsu")

    if config.run_channels_fixed:
        save_results(fixed_thresholding_pipeline(datasets, config),
                     config,
                     f"{tstr}_ft")

    if config.run_channels_percentile:
        save_results(percentile_thresholding_pipeline(datasets, config),
                     config,
                     f"{tstr}_pt")

    if config.run_fesi:
        save_results(base_pipeline(datasets, config, fesi, "fesi"),
                     config,
                     f"{tstr}_fesi")

    if config.run_entropymasker:
        entropymasker_fn = partial(entropy_masker, radius_pre=5, radius_post=10)
        save_results(base_pipeline(datasets, config, entropymasker_fn, "entropy_masker"),
                     config,
                     f"{tstr}_em")


def main():
    config = Config.load('./run_pipelines.yml')
    if config.compute_on_dev_set:
        run_on_datasets(config, True)
    if config.compute_on_test_set:
        run_on_datasets(config, False)


if __name__ == "__main__":
    main()
